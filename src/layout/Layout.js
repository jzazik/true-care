import React from "react";
import "./Layout.css";
import Page from "../components/Page";
import LeftMenu from "../components/LeftMenu";
import HeadMenu from "../components/HeadMenu";

function Layout() {
    return (
        <div>
            <LeftMenu/>
            <HeadMenu/>
            <Page/>
        </div>
    );
}

export default Layout;
