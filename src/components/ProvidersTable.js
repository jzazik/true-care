import React from "react";
import './ProvidersTable.css';
import {Table} from "react-bootstrap";
import StatusCell from "./StatusCell";
import KebabMenu from "./KebabMenu";
import Axios from "axios";

class ProvidersTable extends React.Component {

    typeCell = ({flag, type}) => (
        <div className="type-icons">
            <div className={"type-icon type-icon--" + (flag ? 'flag' : 'noflag')}/>
            <div className={"type-icon type-icon--" + type}/>
        </div>
    );

    phoneCall = (e, {phone}) => {
        e.preventDefault();
        Axios.get('/call/admin/', {params: {phone}})
            .then(function (response) {
                // handle success
                console.log(response);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            });
    };

    render() {
        return (
            <Table responsive borderless>
                <thead>
                <tr>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>ID</th>
                    <th>Status</th>
                    <th/>
                </tr>
                </thead>
                <tbody>
                {this.props.providers.length > 0 && this.props.providers.map(provider => (
                    <tr key={provider.id} className={provider.status === 'cancel' ? 'cancelled' : ''}>
                        <td>{this.typeCell(provider)}</td>
                        <td className="font-weight-bold">{provider.name}</td>
                        <td>{provider.email}</td>
                        <td>{provider.phone}</td>
                        <td>{provider.id}</td>
                        <td>
                            <StatusCell
                                onChange={status => this.props.changeStatus(provider, status)}
                                status={provider.status}
                            />
                        </td>
                        <td className="">
                            <div className="button-cell">
                                <button className="button button--chat">Chat</button>
                                <button onClick={(e) => this.phoneCall(e, provider)}
                                        className="button button--call">Call
                                </button>
                                <KebabMenu onShowFeedback={() => this.props.showFeedbackModal(provider)}/>
                            </div>
                        </td>
                    </tr>
                ))}
                </tbody>
            </Table>
        );
    }
}

export default ProvidersTable;
