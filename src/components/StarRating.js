import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Rating from '@material-ui/lab/Rating';
import "./StarsRating.css"

const StyledRating = withStyles({
    iconFilled: {
        color: '#ff6d75',
    },
    iconHover: {
        color: '#ff3d47',
    },
})(Rating);

export default function StarRating(props) {
    return (
        <div>
            <StyledRating
                onChange={(event, newValue) => {
                    props.onChange(newValue);
                }}
                className={props.className}
                name="customized-color"
                value={props.rating}
                icon={<div className="star star-yellow"/>}
                emptyIcon={<div className="star star-grey"/>}
            />
        </div>
    );
}
