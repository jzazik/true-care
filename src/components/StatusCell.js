import React from "react";
import {Dropdown} from "react-bootstrap";
import "./StatusCell.css";
import Portal from "./Portal";


class StatusCell extends React.Component {

    constructor(props) {
        super(props);
        this.ref = React.createRef();

        this.state = {
            dropdownStyle: {}
        };
    }

    statusLine = (status, textClass = '', ref = null, onClick = null) => {
        const textClassNames = `status-name ${textClass}`;

        return (
            <div
                ref={ref}
                className={"status status--" + status}
            >
                <span
                    className={textClassNames}
                    onClick={onClick}
                >
                    {this.statusTextDictionary[status]}
                </span>
            </div>
        );
    };

    statusTextDictionary = {
        contacting: 'Contacting',
        talked: 'Talked to the client',
        assessment: 'Assessment scheduled',
        signed: 'Contract Signed',
        cancel: 'Cancel the client',
    };

    customToggle = React.forwardRef(({children, onClick}, ref) => (
        this.statusLine(this.props.status, 'status-name--underline', ref, (e) => {
            e.preventDefault();
            onClick(e);
        })
    ));

    customMenu = React.forwardRef(
        ({children, style, className, 'aria-labelledby': labeledBy}, ref) => {
            // const [value, setValue] = useState('');

            return (
                <div
                    ref={ref}
                    className={className}
                    aria-labelledby={labeledBy}
                >
                    {children}
                </div>
            );
        },
    );

    componentDidMount() {
        this.rect = this.ref.current.getBoundingClientRect();
        this.setState({
            dropdownStyle: {
                position: 'absolute',
                top: this.rect.top,
                left: this.rect.left
            }
        });

    }

    render() {
        return (
            <Dropdown ref={this.ref}>
                <Dropdown.Toggle as={this.customToggle}>
                    {this.statusTextDictionary[this.props.status]}
                </Dropdown.Toggle>
                <Portal>
                    <div style={this.state.dropdownStyle}>
                        <Dropdown.Menu as={this.customMenu} className="dropdown-status">
                            {Object.keys(this.statusTextDictionary).map(item => (
                                <Dropdown.Item
                                    key={item}
                                    onClick={() => {
                                        this.props.onChange(item);
                                    }}
                                >
                                    {this.statusLine(item)}
                                </Dropdown.Item>
                            ))}
                        </Dropdown.Menu>
                    </div>
                </Portal>
            </Dropdown>
        );
    }
}

export default StatusCell;

