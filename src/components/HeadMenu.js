import React from "react";
import {InputGroup, FormControl, Button} from "react-bootstrap";
import "./HeadMenu.css";

class HeadMenu extends React.Component {
    render() {
        return (
            <header className="head-menu">
                <div className="head-menu__left">
                    <a href="/" className="head-menu__left-link head-menu__left-link--back text-uppercase">Back</a>
                    <InputGroup className="head-menu__input">
                        <FormControl
                            placeholder="Go to case #ID"
                            className="head-menu__input-form"
                        />
                        <InputGroup.Append>
                            <Button className="head-menu__input-button"/>
                        </InputGroup.Append>
                    </InputGroup>
                    <a href="/" className="head-menu__left-link head-menu__left-link--kpi">Fulfillment KPIs</a>
                </div>
                <div className="head-menu__right">
                    <a href="/" className="head-menu__right-link">leo@truecare24.com</a>
                </div>
            </header>
        );
    }
}

export default HeadMenu;
