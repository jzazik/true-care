import React from "react";
import './FeedbackModal.css';
import {Modal, Form} from "react-bootstrap";
import StarRating from "./StarRating";

class FeedbackModal extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rating: props.feedback.rating,
            text: props.feedback.text
        };
    }

    render() {
        return (
            <Modal
                onHide={this.props.hide}
                dialogClassName="feedback-modal"
                show
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Set the rating and provide
                        the feedback note:
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <StarRating
                            rating={this.state.rating}
                            onChange={rating => this.setState({rating})}
                            className="feedback-modal-stars"
                        />
                        <Form.Control as="textarea" rows="6" placeholder="Type here"
                                      value={this.state.text}
                                      className="feedback-modal-input"
                                      onChange={(e) => this.setState({text: e.target.value})}
                        />
                        <button onClick={(e) => {
                            this.props.saveFeedback(e, this.state.rating, this.state.text);
                        }} className="button feedback-modal-save">Save
                        </button>
                    </Form>
                </Modal.Body>
            </Modal>
        );
    }
}

export default FeedbackModal;
