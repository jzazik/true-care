import React from "react";
import './Page.css';
import ProvidersTable from "./ProvidersTable";
import FeedbackModal from "./FeedbackModal";

class Page extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            providers: [],
            feedbackModalShow: false,
            feedbackModalAccountId: null,
            feedback: {
                text: '',
                rating: 0,
            }
        };
    }

    componentDidMount() {
        this.fetchData().then(providers => this.setState({providers}));
    }

    fetchData = async () => {
        const response = await fetch('/data/providers.json');
        return await response.json();
    };

    changeStatus = ({id}, status) => {
        this.setState({
            providers: this.state.providers.map(el => (el.id === id ? {...el, status} : el))
        });
    };

    showFeedbackModal = ({id}) => {
        const provider = this.state.providers.find(provider => provider.id === id);

        this.setState({
            feedbackModalShow: true,
            feedbackModalAccountId: id,
            feedback: {
                text: provider ? provider.feedback.text : '',
                rating: provider ? provider.feedback.rating : null,
            }
        });
    };

    saveFeedback = (e, rating, text) => {
        e.preventDefault();
        const provider = this.state.providers.find(provider => provider.id === this.state.feedbackModalAccountId);

        const newEl = {
            ...provider,
            feedback: {text, rating}
        };

        this.setState({
            providers: this.state.providers.map(el => (el.id === this.state.feedbackModalAccountId ? newEl : el)),
            feedbackModalShow: false
        });
    };

    render() {
        return (
            <main id="main" className="main">
                <h1 className="table-header">Matched providers</h1>
                {this.state.providers.length > 0 &&
                <ProvidersTable
                    providers={this.state.providers}
                    changeStatus={this.changeStatus}
                    showFeedbackModal={this.showFeedbackModal}
                />
                }
                {this.state.feedbackModalShow &&
                <FeedbackModal
                    hide={() => this.setState({feedbackModalShow: false})}
                    feedback={this.state.feedback}
                    saveFeedback={this.saveFeedback}
                />
                }
            </main>
        );
    }
}

export default Page;
