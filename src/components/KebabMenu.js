import React from "react";
import {Dropdown} from "react-bootstrap";
import Portal from "./Portal";
import "./KebabMenu.css";


class KebabMenu extends React.Component {

    constructor(props) {
        super(props);
        this.ref = React.createRef();

        this.state = {
            dropdownStyle: {}
        };
    }

    customToggle = React.forwardRef(({children, onClick, className}, ref) => (
        <div ref={ref}
             className="dropdown-kebab"
             onClick={onClick}
        />
    ));

    customMenu = React.forwardRef(
        ({children, style, className, 'aria-labelledby': labeledBy}, ref) => {
            return (
                <div
                    ref={ref}
                    className={className}
                    aria-labelledby={labeledBy}
                >
                    {children}
                </div>
            );
        },
    );

    componentDidMount() {
        this.rect = this.ref.current.getBoundingClientRect();
        this.setState({
            dropdownStyle: {
                position: 'absolute',
                top: this.rect.top,
                left: this.rect.left,
                transform: "translateX(-110px)",
            }
        });

    }

    render() {
        return (
            <Dropdown ref={this.ref}>
                <Dropdown.Toggle as={this.customToggle}/>
                <Portal>
                    <div style={this.state.dropdownStyle}>
                        <Dropdown.Menu as={this.customMenu} className="dropdown-kebab-menu">
                            <Dropdown.Item>Send intro</Dropdown.Item>
                            <Dropdown.Item onClick={this.props.onShowFeedback}>Provide feedback</Dropdown.Item>
                            <Dropdown.Item>Send reminder</Dropdown.Item>
                            <Dropdown.Item>Background check</Dropdown.Item>
                        </Dropdown.Menu>
                    </div>
                </Portal>
            </Dropdown>
        );
    }
}

export default KebabMenu;

