import React from "react";
import './LeftMenu.css';

class LeftMenu extends React.Component{
    render() {
        return (
            <aside className="left-menu">
                <a href="/" className="left-menu__item left-menu__item--logo"> </a>
                <a href="/" className="left-menu__item left-menu__item--dashboard">Dashboard</a>
                <a href="/" className="left-menu__item left-menu__item--faq">FAQ</a>
                <a href="/" className="left-menu__item left-menu__item--sign-out">Sign out</a>
            </aside>
        );
    }
}

export default LeftMenu;
